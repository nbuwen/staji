Staji
=====


Install
-------

- `git clone https://gitlab.com/nbuwen/staji`
- `cd staji`
- `pip install .`

Usage
-----

```
usage: Standalone Jinja2 Builder [-h] [-s SOURCE] [-t TARGET] [-i IGNORE] [-w]
                                 [FILES [FILES ...]]

positional arguments:
  FILES                 List of files to compile. Default is all files in
                        $source.

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        Source directory. Must exist! Default is cwd.
  -t TARGET, --target TARGET
                        Target directory. Will be created. Can be a path
                        relative to $source or an absolute path. Default is
                        '$source/build'
  -i IGNORE, --ignore IGNORE
                        Set the ignore prefix. All files beginning with
                        $ignore will be ignored. Default is '_'
  -w, --watch           Enable watch mode. The programm will detect changes
                        and recompile automatically.
```
from setuptools import setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(
    name='staji',
    version='0.1',
    description="Standalone Jinja Builder",
    long_description=readme(),
    url='https://gitlab.com/nbuwen/staji',
    author="Niels Buwen",
    license='MIT',
    packages=['staji'],
    zip_safe=False,
    install_requires=[
        'jinja2'
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: MacOS',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Code Generators',
        'Topic :: Text Processing'
    ],
    keywords='static html jinja2 cli',
    entry_points= {
        'console_scripts': ['staji=staji.__main__:main']
    }
)

from os import getcwd, listdir, makedirs
from os.path import isabs, join, expanduser, isdir, isfile
from glob import iglob
from re import search
from traceback import print_stack

from jinja2 import Environment, FileSystemLoader, TemplateSyntaxError, TemplateNotFound, TemplateError

from .extra import all_extras, reset

class ListFilter:
    def __init__(self, files):
        self.files = set(files.split(',')) if files else set()

    def __call__(self, file):
        return file in self.files

    def __iter__(self):
        return iter(self.files)


class GlobFilter:
    def __init__(self, glob):
        self.glob = glob

    def __iter__(self):
        return iglob(self.glob)


class RegexFilter:
    def __init__(self, regex):
        self.regex = regex

    def __iter__(self):
        return (file for file in listdir('.') if search(self.regex, file))


FILTERS = {
        'L': ListFilter,
        'G': GlobFilter,
        'R': RegexFilter
    }


def make_filter(pattern):
    try:
        return FILTERS[pattern[0]](pattern[1:])
    except (KeyError, IndexError):
        return None


class BuildConfig:
    def __init__(self, path=None, target='build', input_="", dependencies=None):
        self.path = expanduser(path or getcwd())
        target = expanduser(target)
        self.target = target if isabs(target) else join(self.path, target)

        self.input = make_filter(input_)
        self.dependencies = make_filter(dependencies)

    def setup(self):
        if not isdir(self.path):
            raise RuntimeError(f"Source path {self.path!r} does not exist!")

        if not isdir(self.target):
            makedirs(self.target, exist_ok=True)
        else:
            raise RuntimeError(f"Target path {self.target!r} does not exist!")

        if self.input is None:
            raise RuntimeError(f"Filter is None")
        if self.dependencies is None:
            raise RuntimeError(f"Dependency filter is None")


class Builder:
    def __init__(self, config: BuildConfig):
        self.config = config
        self.env = Environment(loader=FileSystemLoader(config.path))

    @staticmethod
    def _iterate_files(filter_):
        return (file for file in filter_ if not isdir(file))

    def list_files(self):
        return self._iterate_files(self.config.input)

    def list_dependencies(self):
        return self._iterate_files(self.config.dependencies)

    def compile_and_save(self, file):
        reset()
        try:
            template = self.env.get_template(file)
            rendered = template.render(file=file, **all_extras)
        except TemplateSyntaxError as e:
            print("Syntax Error")
            print("Line:", e.lineno)
            print(e.message)
            return 'render-error', e
        except TemplateNotFound as e:
            print("No Found")
            print(e.name)
            return 'render-error', e
        except TemplateError as e:
            print("Error")
            print(e.message)
            return 'render-error', e

        try:
            with open(join(self.config.target, file), 'w') as fout:
                fout.write(rendered)
        except IOError as e:
            return 'io-error', e

        return 'ok', None

from argparse import ArgumentParser
from sys import argv
from collections import defaultdict
from os import stat
from time import sleep

from . import Builder, BuildConfig


parser = ArgumentParser('Standalone Jinja2 Builder')
parser.add_argument(
    '-s', '--source',
    dest='source',
    default=None,
    help="Source directory. Must exist! Default is cwd.")
parser.add_argument(
    '-t', '--target',
    dest='target',
    default='build',
    help="Target directory. Will be created."
    " Can be a path relative to $source or an absolute path."
    " Default is '$source/build'"
)
parser.add_argument(
    '-w', '--watch',
    dest='watch',
    default=False,
    action='store_true',
    help="Enable watch mode. The programm will detect changes and"
    " recompile automatically."
)
parser.add_argument(
    '-i', '--input',
    dest='input',
    default='G*',
    help="Regex or glob to match the files. Default is all files."
         " Use 'L' for a list of files: \"Lfoo.html,bar.html\""
         " Use 'R' for a regex: \"R(foo|html)\\.html\""
         " Use 'G' for a glob: \"G*.html\""
)
parser.add_argument(
    '-d', '--dependencies',
    dest='dependencies',
    default='L',
    help="Specify files that should not directly be compiled but are dependencies of other files."
         " Use the same syntax as for FILES. Only necessary for WATCH mode"
)


def get_mod_time(file):
    return stat(file).st_mtime


CODE_TO_SIGN = {
    'ok': '.',
    'io-error': 'o',
    'render-error': 'e'
}


def run_single_shot(builder):
    results = {code: [] for code in CODE_TO_SIGN}

    for file in builder.list_files():
        code, error = builder.compile_and_save(file)
        results[code].append((file, error))
        print(CODE_TO_SIGN[code], end='', flush=True)
    print()

    for code, items in results.items():
        if items:
            print(code)
            for file, error in items:
                print(f"- {file:<15}: {error or ''}")


def run_watch_mode(builder):
    last_change = defaultdict(int)
    special_files = []

    print("watching (CTRL+C to exit) ...")
    try:
        while True:
            new_special_files = [get_mod_time(file) for file in builder.list_dependencies()]
            if new_special_files != special_files:
                last_change = defaultdict(int)
                special_files = new_special_files

            for file in builder.list_files():
                time = get_mod_time(file)

                if time > last_change[file]:
                    last_change[file] = time
                    code, error = builder.compile_and_save(file)
                    print(f"- {file:<15}: {code} {error or ''}")

            sleep(0.1)
    except KeyboardInterrupt:
        print('\rstopped')


def main(args=None):
    if args is None:
        args = argv[1:]

    ns = parser.parse_args(args)

    config = BuildConfig(path=ns.source, target=ns.target, input_=ns.input, dependencies=ns.dependencies)
    builder = Builder(config)

    if ns.watch:
        run_watch_mode(builder)
    else:
        run_single_shot(builder)


if __name__ == '__main__':
    main()

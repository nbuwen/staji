def count(identifier, initial=0):
    value = count.values.setdefault(identifier, initial)
    count.values[identifier] += 1
    return value
count.values = {}


all_extras = {
    'count': count
}


def reset():
    count.values.clear()
